var app = app || {};

app.init = function () {

  app.toTop();
  app.switchMenu();

}

/*
 * button to top
 */
app.toTop = function() {
  var module = this;
  module.$btn = $("#to-top-btn"); //selector for to top button
  module.init = function() {
    module.$btn.click(module.on_click);
  }
  module.on_click = function() {
    $("html, body").animate({scrollTop: 0}, module.duration);
  }
  if( module.$btn.length > 0 ) module.init();
}

/*
 * switch menu navigation
 */
app.switchMenu = function () {
  var $btn = $('#navbar-toggle'), $target = $('#navi');
  $btn.click(function() {
    $target.stop().slideToggle(function() {
      app.navFit();
    });
    disableScroll(true);
  });
  var $btnClose = $('#btn-close');
  $btnClose.click(function() {
    $target.stop().slideToggle();
    disableScroll(false);
  })
}

$(function() {

  app.init();

});