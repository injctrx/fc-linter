'use strict';

module.exports = function(grunt) {

  grunt.initConfig({

    htmllint: {
      default_options: {
        options: {
          'force': true,
          'id-class-style': 'dash',
          'indent-style': 'spaces',
          'attr-name-style': 'dash',
          'indent-width': 2,
          'line-end-style': false,
          'tag-bans': ['style'],
        },
        src: ['files/**/*.html']
      }
    },

    csslint: {
      default_options: {
        options: {
          'force': true,
          'import': 1,
          'vendor-prefix': 2,
          'order-alphabetical': false,
          'box-model': false,
          'adjoining-classes': false,
          'important': false,
          'box-sizing': false,
          'gradients': false,
          'universal-selector': false,
          'star-property-hack': false,
          'fallback-colors': false,
          'overqualified-elements': false,
          'duplicate-background-images': false,
          'text-indent': false,
          'floats': false,
          'outline-none': false,
          'font-sizes': false,
        },
        src: ['files/**/*.css']
      }
    },

    jshint: {
      default_options: {
        options: {
          'force': true
        },
        src: ['files/**/*.js']
      },
    },

  });

  grunt.loadNpmTasks('grunt-htmllint');
  grunt.loadNpmTasks('grunt-contrib-csslint');
  grunt.loadNpmTasks('grunt-contrib-jshint');

  grunt.registerTask('default', ['html', 'css', 'js']);

  grunt.registerTask('html', ['htmllint']);
  grunt.registerTask('css', ['csslint']);
  grunt.registerTask('js', ['jshint']);

};